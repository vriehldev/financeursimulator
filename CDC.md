# Cahier des charges 

## Description des objectifs pédagogiques du jeu

### Objectif pédagogique général

Le joueur doit apprendre comment une organisation choisit un projet culturel européen à financer en fonction de plusieurs facteurs (budget, durée, pertinence du projet, qualité de la conception et de la mise en oeuvre du projet, qualité de l’équipe du projet et des modalités de coopération, impact et dissémination du projet)  

### Description des objectifs pédagogiques

Faire un choix en fonction de différents facteurs pour le financement ou non d’un projet  : 
- Le thème du projet
- Le coût du projet
- Le financement minimum du projet
- La durée du projet
- Le nombre minimum de partenaires associés au projet
- La longueur du dossier
- Le nombre minimum de membres participant au projet

Connaître les différences entre les différents organismes de financements 
- Les types de projets acceptés sont différents en fonction des organismes de financement.  
- La durée du projet nécessaire pour être acceptée est différente en fonction des organismes

## Description du jeu

- **Type de jeu** : jeu de simulation
- **Incarnation du joueur** : un membre d’une organisation de financement responsable du choix des projets à financer

### Déroulement d’une partie

La partie commence par le choix de l’organisation. Pour chaque tour, le joueur peut éliminer ou retenir plusieurs demandes de financement d’un projet culturel selon plusieurs critères qu’il découvre au fur et à mesure de la partie. A la fin du tour, le joueur a un récapitulatif des de ses réponses justes et ses erreurs. Le jeu est fini lorsque le joueur a fait 3 erreurs ou à la fin du 5ème tour de jeu, chaque tour comportant 10 projets.

### Paramétrage d’une partie

Description des options permettant de paramétrer une partie : 
- organisation : l’organisation choisie par le joueur 

## Modèle conceptuel applicatif
Liste, MCD ou diagramme de classe décrivant le jeu, et en particulier les entités, en séparant ce qui est exposé au joueur de ce qui est interne au jeu.

- **Organisation** : une des organisations que le joueur doit choisir en début de partie
	- Durée : nombre de mois requis pour un projet
	- Budget : budget autorisé des projets 
	- Thèmes : thèmes des projets financés par l'organisation
    - Membres : nombre de membres requis pour un projet
    - Partenaires : nombre de partenaires requis pour un projet
    - Zone géographique : zone géographique d’action de l’organisation
    - Longueur du dossier requise pour un projet

- **Projets** : un projet proposé à une organisation
 	- Thème : Thème sur lequel est porté le projet
	- Durée : Nombre de mois nécessaire pour la réalisation du projet
	- Coût : Coût nécessaire pour la réalisation du projet
	- Financement : Pourcentage du coût financé par l’organisation
	- Nombre de membres : Nombre de participants au projet
	- Partenaires : nombre de partenaires du projet
	- Longueur du dossier : Nombre de page du projet
	- Statut : statut des demandeurs de financement
	
- **Tour** : un tour du jeu, durant lequel le joueur doit trier des projets selon un critère
	- Consignes : critères de sélection des projets
	- Projet courant : le projet actuel qu’il faut retenir ou éliminer




## Description des fonctionnalités 

### Entrées

Liste des actions possibles par le joueur. Peut être organisé en module.

**Gestion des tours**
- Fin du tour : passe au formulaire récapitulatif du tour, montrant les réponses justes et les réponses fausses du joueur
- Tour suivant : passe au tour suivant, change le critère de sélection 
**Gestion des tâches** 
- Retenir : retient le projet parmi ceux potentiellement financés par l’organisation
- Eliminer : refus du financement du projet : le projet est éliminé
- Projet suivant : valide le choix du joueur pour le projet courant et passe au projet suivant

### Sorties
Liste des informations présentées au joueurs. Peut être organisé en module.

**Gestion des tours**
- NbProjets : le nombre total de projets sur ce tour
- ProjetCourant : le projet en cours d’évaluation
**Gestion des tâches**
-Consigne : la(les) consigne(s) du tour actuel
-NbErreurs : le nombre d’erreurs commises par le joueur au cours de la partie

### Moteur interne
Liste des interactions entre entrées et sorties

**Gestion des tours**
- Éliminer : 
    - Projet: le projet est refusé 
- Retenir: 
    - Projet: le projet est accepté 
- Passage au projet suivant:
    - ProjetCourant++
- Passage au round suivant:
    - ProjetCourant : le projet courant est réinitialisé à 1 pour pouvoir refaire un tour entier
    - Consigne : un critère de sélection en plus est ajouté, augmentant la difficulté de retenir un projet, et demandant au joueur plus d’attention

**Gestion des tâches** 
- Ajouter une consigne modifie la façon dont le jeu vérifie si le joueur commet une erreur
- Le nombre d’erreurs commises par le joueur n’est pas réinitialisé d’un tour à l’autre

## Scénarios

### Scénario tutorial
Décrire en détail un scénario qui s’appuie sur toutes les fonctionnalités mais sans difficulté pour le joueur.

Tutorial pour comprendre le fonctionnement du jeu et la manière dont les financeurs sélectionnent les projets financés.
Paramètres :
- Nombre de tours : 2
- Nombre de projets par tour : 5, générés de manière fixe (seront toujours les mêmes) et ne changent pas d’un tour à l’autre pour le tutoriel
- Consignes : une consigne ajoutée au début de chaque tour

**Déroulement gagnant :**
Tour 1 : Consigne : sélectionner les projets selon le thème
1. Le joueur prend connaissance des critères de sélection de l’organisation de financement Tutoriel : le thème des projets à retenir est Littérature.
2. Le joueur élimine le premier projet, qui avait un thème ne correspondant pas à l’organisation de financement Tutoriel (Barbecue du dimanche).
[<nbErreurs, 0>]
3. Le joueur élimine le deuxième projet,qui avait un thème ne correspondant pas à l’organisation de financement Tutoriel (Festival musical européen)
[<nbErreurs, 0>]
4. Le joueur retient le troisième projet, qui avait un thème correspondant aux critères de l’organisation de financement Tutoriel (Poésie contemplative européenne)
[<nbErreurs, 0>]
5. Le joueur élimine le quatrième projet,qui avait un thème ne correspondant pas à l’organisation de financement Tutoriel (Safari au Sénégal).
[<nbErreurs, 0>]
6. Le joueur retient le cinquième projet, qui avait un thème correspondant aux critères de l’organisation de financement Tutoriel (Livre pour adolescents et enfants d’europe).
[<nbErreurs, 0>]
7. Le joueur a terminé le tour 1 sans faire d’erreur et arrive sur la page du récapitulatif, puis passe au tour suivant

Tour 2 : Consigne : sélectionner les projets selon le thème et selon le coût  (Littérature et Coût compris entre 50 000€ et 200 000€)
8. Le joueur retient uniquement le troisième projet (Poésie contemplative européenne, 80 000€) car c’est le seul projet répondant à la fois au  critère de thème et au critère de coût de l’organisation de financement Tutoriel.
[<nbErreurs, 0>]
9. Le joueur remporte la partie en n’ayant fait aucune erreur.

**Déroulement perdant :**
Tour 1 : Consigne : sélectionner les projets selon le thème
1. Le joueur retient le premier projet, qui avait un thème ne correspondant pas à l’organisation de financement Tutoriel.
[<nbErreurs, 1>]
2. Le joueur retient le deuxième projet,qui avait un thème ne correspondant pas à l’organisation de financement Tutoriel.
[<nbErreurs, 2>]
3. Le joueur élimine le troisième projet, qui avait un thème correspondant aux critères de l’organisation
[<nbErreurs, 3>]

Le joueur ayant commis 3 erreurs, la partie se termine sur un Game Over.

### Scénarios complémentaires
Décrire moins précisément des idées de scénarios.
1. Scénario des autres organisations :
Paramètres :
- Nombre de tours : 5
- Nombre de projets par tour : 10, générés aléatoirement au début de chaque tour
- Consignes : une consigne ajoutée au début de chaque tour
2. Scénario dans lequel l’organisation choisie a un budget maximal de financement (voir premier point des fonctionnalités additionnelles)

### Fonctionnalités additionnelles
Décrire ici les idées de fonctionnalités additionnelles. Cette partie ne doit servir qu’en dernier recours, pour transmettre ce qui n’a pas été inclus dans les fonctionnalités faute de temps.

- Ajouter un système de gestion de budget qui change selon le type d’organisation : 
le joueur aurait un seuil maximal de financement pour la totalité des projet : par exemple, si ce seuil est de 500 000 €, le joueur peut accepter de financer 1 projet à 400 000 €, mais pas 2 projets à 300 000 €, car cela ferait 600 000 € au total.
- Ajouter la gestion du risque du contrôle (l’organisation de financement peut contrôler si l’argent a été utilisé comme convenu ou non).
- Ajouter beaucoup d’autres projets dans la base de donnée.
- Implémenter les deux autres organisations (Erasmus + et Interreg) voire d’autres encore.
- Ajouter plus de consignes de sélection et générer leur ordre d’apparition de manière aléatoire.
- Améliorer le tutoriel.
- Améliorer le formulaire donnant les informations des organisations de financeurs. 
- Expliquer en quoi une erreur est une erreur.
- Supprimer un formulaire quand il n'est plus utilisé.
