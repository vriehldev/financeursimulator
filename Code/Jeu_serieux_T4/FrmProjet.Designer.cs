﻿
namespace Jeu_serieux_T4
{
    partial class FrmProjet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmProjet));
            this.btnSuivant = new System.Windows.Forms.Button();
            this.btnRetenir = new System.Windows.Forms.Button();
            this.btnEliminer = new System.Windows.Forms.Button();
            this.iPbRegles = new FontAwesome.Sharp.IconPictureBox();
            this.lblProjetCourant = new System.Windows.Forms.Label();
            this.btnMenu = new System.Windows.Forms.Button();
            this.LblTheme = new System.Windows.Forms.Label();
            this.lblDuree = new System.Windows.Forms.Label();
            this.lblCoutFinancement = new System.Windows.Forms.Label();
            this.lblNbMembre = new System.Windows.Forms.Label();
            this.lblLgDossier = new System.Windows.Forms.Label();
            this.lblNom = new System.Windows.Forms.Label();
            this.lblPaysStatut = new System.Windows.Forms.Label();
            this.lblPartenaires = new System.Windows.Forms.Label();
            this.pcbChoose = new System.Windows.Forms.PictureBox();
            this.iPbErreur1 = new FontAwesome.Sharp.IconPictureBox();
            this.iPbErreur2 = new FontAwesome.Sharp.IconPictureBox();
            this.iPbErreur3 = new FontAwesome.Sharp.IconPictureBox();
            this.iPb1 = new FontAwesome.Sharp.IconPictureBox();
            this.iPb2 = new FontAwesome.Sharp.IconPictureBox();
            this.iPb3 = new FontAwesome.Sharp.IconPictureBox();
            this.lbConsigne = new System.Windows.Forms.ListBox();
            this.lblTour = new System.Windows.Forms.Label();
            this.lblC = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.iPbRegles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbChoose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb3)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSuivant
            // 
            this.btnSuivant.Enabled = false;
            this.btnSuivant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSuivant.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuivant.Location = new System.Drawing.Point(878, 650);
            this.btnSuivant.Name = "btnSuivant";
            this.btnSuivant.Size = new System.Drawing.Size(219, 47);
            this.btnSuivant.TabIndex = 0;
            this.btnSuivant.Text = "Projet suivant";
            this.btnSuivant.UseVisualStyleBackColor = true;
            this.btnSuivant.Click += new System.EventHandler(this.btnSuivant_Click);
            // 
            // btnRetenir
            // 
            this.btnRetenir.BackColor = System.Drawing.Color.LightGreen;
            this.btnRetenir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetenir.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetenir.Location = new System.Drawing.Point(649, 346);
            this.btnRetenir.Name = "btnRetenir";
            this.btnRetenir.Size = new System.Drawing.Size(219, 47);
            this.btnRetenir.TabIndex = 1;
            this.btnRetenir.Text = "Retenir";
            this.btnRetenir.UseVisualStyleBackColor = false;
            this.btnRetenir.Click += new System.EventHandler(this.btnAccepter_Click);
            // 
            // btnEliminer
            // 
            this.btnEliminer.BackColor = System.Drawing.Color.LightCoral;
            this.btnEliminer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminer.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminer.Location = new System.Drawing.Point(649, 428);
            this.btnEliminer.Name = "btnEliminer";
            this.btnEliminer.Size = new System.Drawing.Size(219, 47);
            this.btnEliminer.TabIndex = 2;
            this.btnEliminer.Text = "Eliminer";
            this.btnEliminer.UseVisualStyleBackColor = false;
            this.btnEliminer.Click += new System.EventHandler(this.btnRefuser_Click);
            // 
            // iPbRegles
            // 
            this.iPbRegles.BackColor = System.Drawing.Color.Transparent;
            this.iPbRegles.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPbRegles.IconChar = FontAwesome.Sharp.IconChar.QuestionCircle;
            this.iPbRegles.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPbRegles.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPbRegles.IconSize = 53;
            this.iPbRegles.Location = new System.Drawing.Point(12, 3);
            this.iPbRegles.Name = "iPbRegles";
            this.iPbRegles.Size = new System.Drawing.Size(61, 53);
            this.iPbRegles.TabIndex = 4;
            this.iPbRegles.TabStop = false;
            this.iPbRegles.Click += new System.EventHandler(this.iPbRegles_Click);
            // 
            // lblProjetCourant
            // 
            this.lblProjetCourant.AutoSize = true;
            this.lblProjetCourant.BackColor = System.Drawing.Color.Transparent;
            this.lblProjetCourant.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProjetCourant.Location = new System.Drawing.Point(919, 44);
            this.lblProjetCourant.Name = "lblProjetCourant";
            this.lblProjetCourant.Size = new System.Drawing.Size(169, 35);
            this.lblProjetCourant.TabIndex = 5;
            this.lblProjetCourant.Text = "Projet : 1 / 10";
            // 
            // btnMenu
            // 
            this.btnMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenu.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu.Location = new System.Drawing.Point(12, 650);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(219, 47);
            this.btnMenu.TabIndex = 6;
            this.btnMenu.Text = "Menu principal";
            this.btnMenu.UseVisualStyleBackColor = true;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // LblTheme
            // 
            this.LblTheme.AutoSize = true;
            this.LblTheme.BackColor = System.Drawing.Color.Transparent;
            this.LblTheme.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTheme.Location = new System.Drawing.Point(40, 187);
            this.LblTheme.Name = "LblTheme";
            this.LblTheme.Size = new System.Drawing.Size(0, 58);
            this.LblTheme.TabIndex = 7;
            // 
            // lblDuree
            // 
            this.lblDuree.AutoSize = true;
            this.lblDuree.BackColor = System.Drawing.Color.Transparent;
            this.lblDuree.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuree.Location = new System.Drawing.Point(40, 259);
            this.lblDuree.Name = "lblDuree";
            this.lblDuree.Size = new System.Drawing.Size(0, 58);
            this.lblDuree.TabIndex = 8;
            // 
            // lblCoutFinancement
            // 
            this.lblCoutFinancement.AutoSize = true;
            this.lblCoutFinancement.BackColor = System.Drawing.Color.Transparent;
            this.lblCoutFinancement.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoutFinancement.Location = new System.Drawing.Point(40, 336);
            this.lblCoutFinancement.Name = "lblCoutFinancement";
            this.lblCoutFinancement.Size = new System.Drawing.Size(0, 58);
            this.lblCoutFinancement.TabIndex = 9;
            // 
            // lblNbMembre
            // 
            this.lblNbMembre.AutoSize = true;
            this.lblNbMembre.BackColor = System.Drawing.Color.Transparent;
            this.lblNbMembre.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbMembre.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblNbMembre.Location = new System.Drawing.Point(40, 483);
            this.lblNbMembre.Name = "lblNbMembre";
            this.lblNbMembre.Size = new System.Drawing.Size(0, 58);
            this.lblNbMembre.TabIndex = 11;
            // 
            // lblLgDossier
            // 
            this.lblLgDossier.AutoSize = true;
            this.lblLgDossier.BackColor = System.Drawing.Color.Transparent;
            this.lblLgDossier.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLgDossier.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLgDossier.Location = new System.Drawing.Point(40, 553);
            this.lblLgDossier.Name = "lblLgDossier";
            this.lblLgDossier.Size = new System.Drawing.Size(0, 58);
            this.lblLgDossier.TabIndex = 12;
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.BackColor = System.Drawing.Color.Transparent;
            this.lblNom.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNom.Location = new System.Drawing.Point(40, 59);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(0, 58);
            this.lblNom.TabIndex = 13;
            // 
            // lblPaysStatut
            // 
            this.lblPaysStatut.AutoSize = true;
            this.lblPaysStatut.BackColor = System.Drawing.Color.Transparent;
            this.lblPaysStatut.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaysStatut.Location = new System.Drawing.Point(40, 117);
            this.lblPaysStatut.Name = "lblPaysStatut";
            this.lblPaysStatut.Size = new System.Drawing.Size(0, 58);
            this.lblPaysStatut.TabIndex = 14;
            // 
            // lblPartenaires
            // 
            this.lblPartenaires.AutoSize = true;
            this.lblPartenaires.BackColor = System.Drawing.Color.Transparent;
            this.lblPartenaires.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartenaires.Location = new System.Drawing.Point(40, 404);
            this.lblPartenaires.Name = "lblPartenaires";
            this.lblPartenaires.Size = new System.Drawing.Size(0, 58);
            this.lblPartenaires.TabIndex = 15;
            // 
            // pcbChoose
            // 
            this.pcbChoose.BackColor = System.Drawing.Color.Transparent;
            this.pcbChoose.InitialImage = null;
            this.pcbChoose.Location = new System.Drawing.Point(465, 543);
            this.pcbChoose.Name = "pcbChoose";
            this.pcbChoose.Size = new System.Drawing.Size(119, 111);
            this.pcbChoose.TabIndex = 16;
            this.pcbChoose.TabStop = false;
            this.pcbChoose.Visible = false;
            // 
            // iPbErreur1
            // 
            this.iPbErreur1.BackColor = System.Drawing.Color.Transparent;
            this.iPbErreur1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iPbErreur1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur1.IconChar = FontAwesome.Sharp.IconChar.TimesCircle;
            this.iPbErreur1.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur1.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPbErreur1.IconSize = 66;
            this.iPbErreur1.Location = new System.Drawing.Point(995, 187);
            this.iPbErreur1.Name = "iPbErreur1";
            this.iPbErreur1.Size = new System.Drawing.Size(83, 66);
            this.iPbErreur1.TabIndex = 17;
            this.iPbErreur1.TabStop = false;
            this.iPbErreur1.Visible = false;
            // 
            // iPbErreur2
            // 
            this.iPbErreur2.BackColor = System.Drawing.Color.Transparent;
            this.iPbErreur2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iPbErreur2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur2.IconChar = FontAwesome.Sharp.IconChar.TimesCircle;
            this.iPbErreur2.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur2.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPbErreur2.IconSize = 62;
            this.iPbErreur2.Location = new System.Drawing.Point(995, 259);
            this.iPbErreur2.Name = "iPbErreur2";
            this.iPbErreur2.Size = new System.Drawing.Size(83, 62);
            this.iPbErreur2.TabIndex = 18;
            this.iPbErreur2.TabStop = false;
            this.iPbErreur2.Visible = false;
            // 
            // iPbErreur3
            // 
            this.iPbErreur3.BackColor = System.Drawing.Color.Transparent;
            this.iPbErreur3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iPbErreur3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur3.IconChar = FontAwesome.Sharp.IconChar.TimesCircle;
            this.iPbErreur3.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur3.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPbErreur3.IconSize = 66;
            this.iPbErreur3.Location = new System.Drawing.Point(995, 327);
            this.iPbErreur3.Name = "iPbErreur3";
            this.iPbErreur3.Size = new System.Drawing.Size(83, 66);
            this.iPbErreur3.TabIndex = 19;
            this.iPbErreur3.TabStop = false;
            this.iPbErreur3.Visible = false;
            // 
            // iPb1
            // 
            this.iPb1.BackColor = System.Drawing.Color.Transparent;
            this.iPb1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPb1.IconChar = FontAwesome.Sharp.IconChar.Circle;
            this.iPb1.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPb1.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPb1.IconSize = 66;
            this.iPb1.Location = new System.Drawing.Point(995, 187);
            this.iPb1.Name = "iPb1";
            this.iPb1.Size = new System.Drawing.Size(83, 66);
            this.iPb1.TabIndex = 20;
            this.iPb1.TabStop = false;
            // 
            // iPb2
            // 
            this.iPb2.BackColor = System.Drawing.Color.Transparent;
            this.iPb2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPb2.IconChar = FontAwesome.Sharp.IconChar.Circle;
            this.iPb2.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPb2.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPb2.IconSize = 66;
            this.iPb2.Location = new System.Drawing.Point(995, 255);
            this.iPb2.Name = "iPb2";
            this.iPb2.Size = new System.Drawing.Size(83, 66);
            this.iPb2.TabIndex = 21;
            this.iPb2.TabStop = false;
            // 
            // iPb3
            // 
            this.iPb3.BackColor = System.Drawing.Color.Transparent;
            this.iPb3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPb3.IconChar = FontAwesome.Sharp.IconChar.Circle;
            this.iPb3.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPb3.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPb3.IconSize = 66;
            this.iPb3.Location = new System.Drawing.Point(995, 327);
            this.iPb3.Name = "iPb3";
            this.iPb3.Size = new System.Drawing.Size(83, 66);
            this.iPb3.TabIndex = 22;
            this.iPb3.TabStop = false;
            // 
            // lbConsigne
            // 
            this.lbConsigne.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.lbConsigne.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(211)))), ((int)(((byte)(119)))));
            this.lbConsigne.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbConsigne.Font = new System.Drawing.Font("MV Boli", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConsigne.FormattingEnabled = true;
            this.lbConsigne.ItemHeight = 29;
            this.lbConsigne.Location = new System.Drawing.Point(591, 59);
            this.lbConsigne.Name = "lbConsigne";
            this.lbConsigne.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbConsigne.Size = new System.Drawing.Size(260, 232);
            this.lbConsigne.TabIndex = 23;
            // 
            // lblTour
            // 
            this.lblTour.AutoSize = true;
            this.lblTour.BackColor = System.Drawing.Color.Transparent;
            this.lblTour.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTour.Location = new System.Drawing.Point(919, 9);
            this.lblTour.Name = "lblTour";
            this.lblTour.Size = new System.Drawing.Size(87, 35);
            this.lblTour.TabIndex = 24;
            this.lblTour.Text = "Tour 1";
            // 
            // lblC
            // 
            this.lblC.AutoSize = true;
            this.lblC.BackColor = System.Drawing.Color.Transparent;
            this.lblC.Font = new System.Drawing.Font("MV Boli", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblC.Location = new System.Drawing.Point(585, 25);
            this.lblC.Name = "lblC";
            this.lblC.Size = new System.Drawing.Size(268, 31);
            this.lblC.TabIndex = 26;
            this.lblC.Text = "Retenez les projets : ";
            // 
            // FrmProjet
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1100, 700);
            this.Controls.Add(this.lblC);
            this.Controls.Add(this.lblTour);
            this.Controls.Add(this.lbConsigne);
            this.Controls.Add(this.iPb3);
            this.Controls.Add(this.iPb2);
            this.Controls.Add(this.iPb1);
            this.Controls.Add(this.iPbErreur3);
            this.Controls.Add(this.iPbErreur2);
            this.Controls.Add(this.iPbErreur1);
            this.Controls.Add(this.pcbChoose);
            this.Controls.Add(this.lblPartenaires);
            this.Controls.Add(this.lblPaysStatut);
            this.Controls.Add(this.lblNom);
            this.Controls.Add(this.lblLgDossier);
            this.Controls.Add(this.lblNbMembre);
            this.Controls.Add(this.lblCoutFinancement);
            this.Controls.Add(this.lblDuree);
            this.Controls.Add(this.LblTheme);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.lblProjetCourant);
            this.Controls.Add(this.iPbRegles);
            this.Controls.Add(this.btnEliminer);
            this.Controls.Add(this.btnRetenir);
            this.Controls.Add(this.btnSuivant);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmProjet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.fProjet_Load);
            this.Shown += new System.EventHandler(this.FrmProjet_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.iPbRegles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbChoose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSuivant;
        private System.Windows.Forms.Button btnRetenir;
        private System.Windows.Forms.Button btnEliminer;
        private FontAwesome.Sharp.IconPictureBox iPbRegles;
        private System.Windows.Forms.Label lblProjetCourant;
        private System.Windows.Forms.Button btnMenu;
        private System.Windows.Forms.Label LblTheme;
        private System.Windows.Forms.Label lblDuree;
        private System.Windows.Forms.Label lblCoutFinancement;
        private System.Windows.Forms.Label lblNbMembre;
        private System.Windows.Forms.Label lblLgDossier;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.Label lblPaysStatut;
        private System.Windows.Forms.Label lblPartenaires;
        private System.Windows.Forms.PictureBox pcbChoose;
        private FontAwesome.Sharp.IconPictureBox iPbErreur1;
        private FontAwesome.Sharp.IconPictureBox iPbErreur2;
        private FontAwesome.Sharp.IconPictureBox iPbErreur3;
        private FontAwesome.Sharp.IconPictureBox iPb1;
        private FontAwesome.Sharp.IconPictureBox iPb2;
        private FontAwesome.Sharp.IconPictureBox iPb3;
        private System.Windows.Forms.ListBox lbConsigne;
        private System.Windows.Forms.Label lblTour;
        private System.Windows.Forms.Label lblC;
    }
}