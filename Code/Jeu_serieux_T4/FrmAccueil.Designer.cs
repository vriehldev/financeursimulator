﻿
namespace Jeu_serieux_T4
{
    partial class fAcceuil
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fAcceuil));
            this.btnEuropeCreative = new System.Windows.Forms.Button();
            this.btnErasmus = new System.Windows.Forms.Button();
            this.btnInterreg = new System.Windows.Forms.Button();
            this.iPbRegles = new FontAwesome.Sharp.IconPictureBox();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.btnTuto = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblRegle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.iPbRegles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnEuropeCreative
            // 
            this.btnEuropeCreative.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEuropeCreative.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEuropeCreative.Location = new System.Drawing.Point(405, 415);
            this.btnEuropeCreative.Name = "btnEuropeCreative";
            this.btnEuropeCreative.Size = new System.Drawing.Size(304, 56);
            this.btnEuropeCreative.TabIndex = 0;
            this.btnEuropeCreative.Text = "Europe Créative";
            this.btnEuropeCreative.UseVisualStyleBackColor = true;
            this.btnEuropeCreative.Click += new System.EventHandler(this.btnEuropeCreative_Click);
            // 
            // btnErasmus
            // 
            this.btnErasmus.Enabled = false;
            this.btnErasmus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnErasmus.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnErasmus.Location = new System.Drawing.Point(405, 477);
            this.btnErasmus.Name = "btnErasmus";
            this.btnErasmus.Size = new System.Drawing.Size(304, 56);
            this.btnErasmus.TabIndex = 1;
            this.btnErasmus.Text = "Erasmus +";
            this.btnErasmus.UseVisualStyleBackColor = true;
            // 
            // btnInterreg
            // 
            this.btnInterreg.Enabled = false;
            this.btnInterreg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInterreg.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInterreg.Location = new System.Drawing.Point(405, 539);
            this.btnInterreg.Name = "btnInterreg";
            this.btnInterreg.Size = new System.Drawing.Size(304, 56);
            this.btnInterreg.TabIndex = 2;
            this.btnInterreg.Text = "Interreg Rhin Supérieur";
            this.btnInterreg.UseVisualStyleBackColor = true;
            // 
            // iPbRegles
            // 
            this.iPbRegles.BackColor = System.Drawing.Color.Transparent;
            this.iPbRegles.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPbRegles.IconChar = FontAwesome.Sharp.IconChar.QuestionCircle;
            this.iPbRegles.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPbRegles.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPbRegles.IconSize = 57;
            this.iPbRegles.Location = new System.Drawing.Point(1028, 642);
            this.iPbRegles.Name = "iPbRegles";
            this.iPbRegles.Size = new System.Drawing.Size(60, 57);
            this.iPbRegles.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iPbRegles.TabIndex = 3;
            this.iPbRegles.TabStop = false;
            this.iPbRegles.Click += new System.EventHandler(this.iPbRegles_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuitter.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitter.Location = new System.Drawing.Point(12, 632);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(153, 56);
            this.btnQuitter.TabIndex = 4;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // btnTuto
            // 
            this.btnTuto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTuto.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTuto.Location = new System.Drawing.Point(405, 353);
            this.btnTuto.Name = "btnTuto";
            this.btnTuto.Size = new System.Drawing.Size(304, 56);
            this.btnTuto.TabIndex = 5;
            this.btnTuto.Text = "Tutoriel";
            this.btnTuto.UseVisualStyleBackColor = true;
            this.btnTuto.Click += new System.EventHandler(this.btnTuto_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(184, -50);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(671, 397);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // lblRegle
            // 
            this.lblRegle.AutoSize = true;
            this.lblRegle.BackColor = System.Drawing.Color.Transparent;
            this.lblRegle.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegle.Location = new System.Drawing.Point(836, 653);
            this.lblRegle.Name = "lblRegle";
            this.lblRegle.Size = new System.Drawing.Size(186, 35);
            this.lblRegle.TabIndex = 7;
            this.lblRegle.Text = "Règles du jeu : ";
            // 
            // fAcceuil
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1100, 700);
            this.Controls.Add(this.lblRegle);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnTuto);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.iPbRegles);
            this.Controls.Add(this.btnInterreg);
            this.Controls.Add(this.btnErasmus);
            this.Controls.Add(this.btnEuropeCreative);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "fAcceuil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FAcceuil";
            this.Load += new System.EventHandler(this.fAcceuil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.iPbRegles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEuropeCreative;
        private System.Windows.Forms.Button btnErasmus;
        private System.Windows.Forms.Button btnInterreg;
        private FontAwesome.Sharp.IconPictureBox iPbRegles;
        private System.Windows.Forms.Button btnQuitter;
        private System.Windows.Forms.Button btnTuto;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblRegle;
    }
}

