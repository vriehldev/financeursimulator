﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeu_serieux_T4
{
    public partial class FrmAideOrga : Form
    {
        int organisation;
        public FrmAideOrga(int orga)
        {
            organisation = orga;
            InitializeComponent();
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmAideOrga_Load(object sender, EventArgs e)
        {
            if (organisation == -1)
            {
                lblAide.Text = "Dans ce jeu vous incarnerez un membre d’une organisation ayant pour but de financer " +
                    "différents projets liés à la culture européenne qui vous seront proposés. Dans un premier temps " +
                    "vous devrez choisir une des organisations présentes dans le menu principal." +
                    " Le premier tour du jeu se lancera ensuite et le premier projet vous sera affiché. " +
                    "Votre but sera de décider si ce projet respecte les règles de votre organisation de financement " +
                    "Si votre réponse est fausse, une croix apparaîtra sur votre écran de jeu. " +
                    "Au bout de la troisième erreur, votre partie se terminera automatiquement. " +
                    "Après avoir traité tous les projets du tour, une fenêtre récapitulative vous sera affichée " +
                    "et vous pourrez alors lancer un nouveau round qui ajoute de nouveaux critères de sélection. " +
                    "Une fois tous les tours réussis, vous gagnerez la partie.";
            }
            else if(organisation == 0)
            {
                lblAide.Text = "Au cours du tutoriel, vous devrez repecter les critères suivants :" +(char)13+
                               "- un thème lié à la littérature" + (char)13 +
                               "- un coût compris entre 50 000 et 200 000 €" + (char)13 +
                               "- une durée comprise entre 6 et 24 mois" + (char)13 +
                               "- un financement de moins de 80%"+ (char)13+
                               "- un pays européen" + (char)13 +
                               "- un nombre de partenaires supérieur ou égal à 2";
            }
            else if(organisation == 1)
            {
                lblAide.Text = "Informations sur l'organisation Europe Créative :"+ (char)13 +
                               "- un thème lié à la culture" + (char)13 +
                               "- un coût de moins d'un demi-million d'euros" + (char)13 +
                               "- une durée comprise entre 1 et 2 ans" + (char)13 +
                               "- un financement respectable" + (char)13 +
                               "- un pays européen" + (char)13 +
                               "- un nombre de partenaires supérieur ou égal à 3" + (char)13 +
                               "- un dossier compris entre 8 et 30 pages" + (char)13 +
                               "- un nombre de membres d'au moins 10";
            }

        }

    }
}
