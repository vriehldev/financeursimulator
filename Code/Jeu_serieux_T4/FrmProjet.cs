﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeu_serieux_T4
{
    public partial class FrmProjet : Form
    {
        
        string chcon;
        OleDbConnection connec;
        DataSet ds_projets = new DataSet();
        DataRow[] infoProjet;
        DataRow[] infoOrganisation;

        bool premierErreur;
        bool retenu = true;
        bool perdu = false;
        bool correct = true;
        int nbTour;
        int orga;
        int nbErreur;
        int projetCourant = 1;
        int nbProjet = 10;

        //Liste contenant tous les projets du tour 
        List<List<string>> ListProjet = new List<List<string>>();
        List<int> ListRetenu = new List<int>();
        List<int> ListRejeter = new List<int>();

        //Liste contenant les projets retenus
        List<int> ProjetJuste = new List<int>();
        //Liste contenant les projet non retenus
        List<int> ProjetFaux = new List<int>();


        
        public FrmProjet(int organisation, int nbT, int nbE, bool e)
        {
            nbErreur = nbE;
            premierErreur = e;
            nbTour = nbT;
            orga = organisation;
            InitializeComponent();


        }

        private void fProjet_Load(object sender, EventArgs e)
        {
            //Connection à la base de données 
            chcon = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=.\BDDT4.mdb";
            connec = new OleDbConnection();
            connec.ConnectionString = chcon;

            try
            {

                connec.Open();

                DataTable schemaTable = connec.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                for (int i = 0; i < schemaTable.Rows.Count; i++)
                {
                    string nomTable = schemaTable.Rows[i][2].ToString();
                    string requete = "select * from " + nomTable;

                    //objet command
                    OleDbCommand cmd = new OleDbCommand(requete, connec);
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(ds_projets, nomTable);
                    
                }

                if (orga == 0)
                {
                    nbProjet = 5;
                    lblProjetCourant.Text="Projet : 1 / 5";
                    genProjetTuto();
                    
                }
                else
                {
                    //On génére les projets aléatoirement
                    generationProjet();
                }
                
                //On appelle la fonction de mise à jour
                miseAJour();

                if (nbTour >= 1)
                {
                    lbConsigne.Items.Add("- ayant un thème approprié");
                }
                if (nbTour >= 2)
                {                    
                    lbConsigne.Items.Add("- dont le budget correspond");
                    lbConsigne.Items.Add("  à l'organisation");
                }
                if (nbTour >= 3)
                {
                    lbConsigne.Items.Add("- la durée correspond");
                    lbConsigne.Items.Add("  à l'organisation");
                }
                if(nbTour >= 4)
                {
                    lbConsigne.Items.Add("- le nombre de partenaires");
                    lbConsigne.Items.Add("  correspond à l'organisation");
                }
                if(nbTour >= 5)
                {
                    lbConsigne.Items.Add("- la longueur du dossier");
                    lbConsigne.Items.Add("  correspond à l'organisation");
                }

                miseAJourErreur();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }      
        }

        private void genProjetTuto()
        {
            List<List<string>> Lp = new List<List<string>>();

            infoProjet = ds_projets.Tables["Projets"].Select();
            int duree = 6;
            int cout = 40000;
            int financement = 15;
            int partenaire = 2;
            int membre = 10;
            int page = 3;         

            for (int i=0;i<5;i++)
            {
                List<string> info = new List<string>();
                Lp.Add(info);
                Lp.ElementAt(i).Add(infoProjet[i*3]["nom"].ToString());
                Lp.ElementAt(i).Add(infoProjet[i*3]["pays"].ToString());
                Lp.ElementAt(i).Add("privé");
                Lp.ElementAt(i).Add(infoProjet[i*3]["theme"].ToString());
                Lp.ElementAt(i).Add((duree*(i+1)).ToString());
                Lp.ElementAt(i).Add((cout*(i+1)).ToString());
                Lp.ElementAt(i).Add((financement * (i + 1)).ToString());
                Lp.ElementAt(i).Add((partenaire * (i + 1)).ToString());
                Lp.ElementAt(i).Add((membre* (i + 1)).ToString());
                Lp.ElementAt(i).Add((page * (i + 1)).ToString());
                Lp.ElementAt(i).Add((i*3 + 1).ToString());
            }

            ListProjet.Add(Lp.ElementAt(2));
            ListProjet.Add(Lp.ElementAt(4));
            ListProjet.Add(Lp.ElementAt(1));
            ListProjet.Add(Lp.ElementAt(3));
            ListProjet.Add(Lp.ElementAt(0));

            nbProjet = 5;
        }

        private void generationProjet()
        {
            //Recherche des informations sur les décisions se déroulant pendant le jour actuel
            infoProjet = ds_projets.Tables["Projets"].Select();

            List<string> nomDejaUtilise = new List<string>();
            Random rnd = new Random();
            int j;
            int m=1;

            j = rnd.Next(0, infoProjet.Length);
            nomDejaUtilise.Add(infoProjet[j]["nom"].ToString());

            while (m<nbProjet)
            {
                j = rnd.Next(0, infoProjet.Length);
                string nom = infoProjet[j]["nom"].ToString();
                if (!nomDejaUtilise.Contains(nom))
                {
                    nomDejaUtilise.Add(nom);
                    m++;
                }
            }

            for (int i =0; i<nbProjet;i++)
            {
                ListProjet.Add(new List<string>());
              
                ListProjet.ElementAt(i).Add(nomDejaUtilise.ElementAt(i));
                
                j = rnd.Next(0, infoProjet.Length);
                ListProjet.ElementAt(i).Add(infoProjet[j]["pays"].ToString());

                j = rnd.Next(0, 2);
                string statuts;
                if (j == 0)
                {
                    statuts = "public";
                }
                else
                {
                    statuts = "privé";
                }
                ListProjet.ElementAt(i).Add(statuts);

                int indexTheme = rnd.Next(0, infoProjet.Length);
                ListProjet.ElementAt(i).Add(infoProjet[indexTheme]["theme"].ToString());

                j = rnd.Next(1,31);
                ListProjet.ElementAt(i).Add(j.ToString());

                j = (rnd.Next(5,750))*1000;
                ListProjet.ElementAt(i).Add(j.ToString());

                j = rnd.Next(10,91);
                ListProjet.ElementAt(i).Add(j.ToString());

                j = rnd.Next(1,11);
                ListProjet.ElementAt(i).Add(j.ToString());

                j = rnd.Next(10,201);
                ListProjet.ElementAt(i).Add(j.ToString());

                j = rnd.Next(3,31);
                ListProjet.ElementAt(i).Add(j.ToString());

                ListProjet.ElementAt(i).Add((indexTheme+1).ToString());
              
            }

        }

        private void miseAJourErreur()
        {
            if (nbErreur>=1)
            {
                iPb1.Visible = false;
                iPbErreur1.Visible = true;
            }

            if (nbErreur>=2)
            {
                iPb2.Visible = false;
                iPbErreur2.Visible = true;
            }

            if(nbErreur >= 3)
            {
                iPb3.Visible = false;
                iPbErreur3.Visible = true;
                perdu = true;

                Thread.Sleep(1000);
                
                //Le formulaire d'accueil est affiché
                FrmFin frm = new FrmFin(perdu, nbTour, nbErreur,orga);
                frm.Closed += (s, args) => this.Close();
                frm.ShowDialog();
            }

            if(nbErreur==1 && orga==0 && premierErreur && nbTour==1)
            {
                MessageBox.Show("Vous vous êtes trompé, votre erreur est matérialisée par une croix à droite de votre écran, sur le post-it à moitié visible.", "Tutoriel");
                premierErreur = false;
            }
        }

        private void miseAJour()
        {
            try
            {
                
                lblTour.Text = "Tour : " + nbTour;
                lblNom.Text = "Nom : " + ListProjet.ElementAt(projetCourant-1).ElementAt(0).ToString();
                lblPaysStatut.Text = "Pays : " + ListProjet.ElementAt(projetCourant - 1).ElementAt(1).ToString() + ", statut : " + ListProjet.ElementAt(projetCourant - 1).ElementAt(2).ToString(); 
                LblTheme.Text = ListProjet.ElementAt(projetCourant - 1).ElementAt(3).ToString(); 
                lblDuree.Text = "Durée : " + ListProjet.ElementAt(projetCourant - 1).ElementAt(4).ToString() + " mois";
                lblCoutFinancement.Text = "Coût : " + ListProjet.ElementAt(projetCourant - 1).ElementAt(5).ToString() + " €, Financement : " + ListProjet.ElementAt(projetCourant - 1).ElementAt(6).ToString() + " %";
                lblPartenaires.Text = "Nombre de partenaires : " + ListProjet.ElementAt(projetCourant - 1).ElementAt(7).ToString() + " partenaires";
                lblNbMembre.Text = "Nombre de membres : " + ListProjet.ElementAt(projetCourant - 1).ElementAt(8).ToString() + " membres";
                lblLgDossier.Text = "Longueur du dossier : " + ListProjet.ElementAt(projetCourant - 1).ElementAt(9).ToString() + " pages";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private List<int> convertSplittedString(string array)
        {
            List<string> listString;
            List<int> listInt = new List<int>();
            listString = array.Split(',').ToList();
            for (int i = 0; i < listString.Count; i++)
            {
                listInt.Add(Int32.Parse(listString.ElementAt(i)));
            }
            return listInt;
        }

        private void rechercheErreur()
        {
            correct = true;
            infoOrganisation = ds_projets.Tables["Organisations"].Select();

            string ids="";

            if (nbTour >= 1)
            {
                if (orga == 1)
                {
                     ids = infoOrganisation[0]["id_theme"].ToString();
                }
                else if (orga == 0)
                {
                     ids = infoOrganisation[3]["id_theme"].ToString();
                }
                
                
                List<int> ListId = new List<int>();
                ListId = convertSplittedString(ids);

                if (!ListId.Contains(Int32.Parse(ListProjet.ElementAt(projetCourant - 1).ElementAt(10))))
                {
                    correct = false;
                }              
            }

            if (nbTour >= 2)
            {
                int valeurMin=0;
                int valeurMax=0;

                if (orga == 1)
                {
                    valeurMin = Int32.Parse(infoOrganisation[0]["budget_min"].ToString());
                    valeurMax = Int32.Parse(infoOrganisation[0]["budget_max"].ToString());
                }
                else if (orga == 0)
                {
                    valeurMin = Int32.Parse(infoOrganisation[3]["budget_min"].ToString());
                    valeurMax = Int32.Parse(infoOrganisation[3]["budget_max"].ToString());
                }

                int valeur = Int32.Parse(ListProjet.ElementAt(projetCourant - 1).ElementAt(5).ToString());

                if (valeur < valeurMin || valeur > valeurMax)
                {
                    correct = false;
                    
                }
               
            }

            if (nbTour>=3)
            {
                int valeurMin=0;
                int valeurMax=0;

                if (orga == 1)
                {
                    valeurMin = Int32.Parse(infoOrganisation[0]["duree_min"].ToString());
                    valeurMax = Int32.Parse(infoOrganisation[0]["duree_max"].ToString());
                }
                else if (orga == 0)
                {
                    valeurMin = Int32.Parse(infoOrganisation[3]["duree_min"].ToString());
                    valeurMax = Int32.Parse(infoOrganisation[3]["duree_max"].ToString());
                }

                int valeur = Int32.Parse(ListProjet.ElementAt(projetCourant - 1).ElementAt(4).ToString());

                if (valeur < valeurMin || valeur > valeurMax)
                {
                    correct = false;

                }
            }

            if (nbTour >= 4)
            {
                int valeurMin = 0;
                int valeurMax = 0;

                if (orga == 1)
                {
                    valeurMin = Int32.Parse(infoOrganisation[0]["partenaire_min"].ToString());
                    valeurMax = Int32.Parse(infoOrganisation[0]["partenaire_max"].ToString());
                }
                else if (orga == 0)
                {
                    valeurMin = Int32.Parse(infoOrganisation[3]["partenaire_min"].ToString());
                    valeurMax = Int32.Parse(infoOrganisation[3]["partenaire_max"].ToString());
                }

                int valeur = Int32.Parse(ListProjet.ElementAt(projetCourant - 1).ElementAt(7).ToString());

                if (valeur < valeurMin || valeur > valeurMax)
                {
                    correct = false;

                }
            }

            if (nbTour >= 5)
            {
                int valeurMin = 0;
                int valeurMax = 0;

                if (orga == 1)
                {
                    valeurMin = Int32.Parse(infoOrganisation[0]["longueur_min"].ToString());
                    valeurMax = Int32.Parse(infoOrganisation[0]["longueur_max"].ToString());
                }
                else if (orga == 0)
                {
                    valeurMin = Int32.Parse(infoOrganisation[3]["longueur_min"].ToString());
                    valeurMax = Int32.Parse(infoOrganisation[3]["longueur_max"].ToString());
                }

                int valeur = Int32.Parse(ListProjet.ElementAt(projetCourant - 1).ElementAt(9).ToString());

                if (valeur < valeurMin || valeur > valeurMax)
                {
                    correct = false;

                }
            }
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            try
            {
                //Le formulaire d'accueil est affiché
                fAcceuil frmAcc = new fAcceuil();
                frmAcc.Closed += (s, args) => this.Close();
                frmAcc.ShowDialog();               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSuivant_Click(object sender, EventArgs e)
        {

            //On regarde si le joueur a pris la bonne décision ou non 
            rechercheErreur();

            if (retenu&&correct||!retenu &&!correct)
            {
                ProjetJuste.Add(projetCourant-1);
            }
            else
            {
                ProjetFaux.Add(projetCourant - 1);
                nbErreur++;
            }

            if(retenu)
            {
                ListRetenu.Add(projetCourant - 1);
            }
            else
            {
                ListRejeter.Add(projetCourant - 1);
            }

            pcbChoose.Visible = false;
            btnSuivant.Enabled = false;

            miseAJourErreur();
    
            //S'il s'agit du dernier tour du jeu 
            if (projetCourant == nbProjet)
            {               
                //On affiche le formulaire de fin
                FrmRecap newForm = new FrmRecap(ListProjet, orga, nbTour, nbErreur, ProjetFaux, ProjetJuste, ListRetenu, ListRejeter, premierErreur);
                newForm.Closed += (s, args) => this.Close();
                newForm.ShowDialog();
            }
            else
            {
                //On incrémente le numéro de jour de 1
                projetCourant += 1;
                              
                //On appel la précédure qui met à jour le panel
                miseAJour();

                //On affiche la nouvelle valeur du numéro de jour 
                lblProjetCourant.Text = "Projet : "+ projetCourant + " / "+nbProjet;
            }
        }

        private void btnAccepter_Click(object sender, EventArgs e)
        {
            retenu = true;
            btnSuivant.Enabled = true;
            pcbChoose.SizeMode = PictureBoxSizeMode.StretchImage;
            pcbChoose.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\v.png");
            pcbChoose.Visible = true;
            if(orga == 0 && nbTour==1 && projetCourant==1)
            {
                MessageBox.Show("Vous pouvez maintenant valider votre choix en cliquant sur le bouton projet suivant en bas à droite de votre écran.","Tutoriel");
            }
        }

        private void btnRefuser_Click(object sender, EventArgs e)
        {
            btnSuivant.Enabled = true;
            retenu=false;

            pcbChoose.SizeMode = PictureBoxSizeMode.StretchImage;
            pcbChoose.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\x.png");
            pcbChoose.Visible = true;
            if (orga == 0 && nbTour == 1 && projetCourant == 1)
            {
                MessageBox.Show("Vous pouvez maintenant valider votre choix en cliquant sur le bouton projet suivant en bas à droite de votre écran.","Tutoriel");
            }

        }

        private void iPbRegles_Click(object sender, EventArgs e)
        {
            Form formBackground = new Form();
            try
            {
                using (FrmAideOrga uu = new FrmAideOrga(orga))
                {
                    formBackground.StartPosition = FormStartPosition.Manual;
                    formBackground.FormBorderStyle = FormBorderStyle.None;
                    formBackground.Opacity = .50d;
                    formBackground.BackColor = Color.Black;
                    formBackground.WindowState = FormWindowState.Maximized;
                    formBackground.Location = this.Location;
                    formBackground.ShowInTaskbar = false;
                    formBackground.Show();

                    uu.Owner = formBackground;
                    uu.ShowDialog();

                    formBackground.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                formBackground.Dispose();
            }
        }

        private void FrmProjet_Shown(object sender, EventArgs e)
        {        
            if (orga == 0 && nbTour == 1)
            {
                MessageBox.Show("Le but du jeu est de retenir ou de rejeter des projets demandant un financement venant de notre organisation.", "Tutoriel");
                MessageBox.Show("Vous pouvez voir sur la feuille à gauche les caractéristiques du projet en cours d'évaluation.", "Tutoriel");
                MessageBox.Show("Le post-it en haut à droite vous indique les critères à prendre en compte pour l'évaluation du projet durant ce tour.", "Tutoriel");
                MessageBox.Show("Vous pouvez accéder à tout moment aux critères d'évalution de votre organisation en cliquant sur le point d'interrogation en haut à gauche de l'écran.", "Tutoriel");
                MessageBox.Show("Vous pouvez à présent retenir ou refuser le projet en cliquant sur les boutons Retenir ou Eliminer pour faire votre choix.", "Tutoriel");
            }
            if (orga == 0 && nbTour == 2)
            {
                MessageBox.Show("Vous êtes passé au tour suivant, une nouvelle consigne est apparue. N'acceptez que les projets répondant à tous les critères ! ", "Tutoriel");
            }
        }
    }
}
