﻿namespace Jeu_serieux_T4
{
    partial class FrmRecap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRecap));
            this.btnSuivant = new System.Windows.Forms.Button();
            this.lbProjetJuste = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPartenaires = new System.Windows.Forms.Label();
            this.lblPaysStatut = new System.Windows.Forms.Label();
            this.lblNom = new System.Windows.Forms.Label();
            this.lblLgDossier = new System.Windows.Forms.Label();
            this.lblNbMembre = new System.Windows.Forms.Label();
            this.lblCoutFinancement = new System.Windows.Forms.Label();
            this.lblDuree = new System.Windows.Forms.Label();
            this.LblTheme = new System.Windows.Forms.Label();
            this.lblJuste = new System.Windows.Forms.Label();
            this.lblFaux = new System.Windows.Forms.Label();
            this.lbProjetFaux = new System.Windows.Forms.ListBox();
            this.pcbChoose = new System.Windows.Forms.PictureBox();
            this.iPb3 = new FontAwesome.Sharp.IconPictureBox();
            this.iPb2 = new FontAwesome.Sharp.IconPictureBox();
            this.iPb1 = new FontAwesome.Sharp.IconPictureBox();
            this.iPbErreur3 = new FontAwesome.Sharp.IconPictureBox();
            this.iPbErreur2 = new FontAwesome.Sharp.IconPictureBox();
            this.iPbErreur1 = new FontAwesome.Sharp.IconPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pcbChoose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSuivant
            // 
            this.btnSuivant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSuivant.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuivant.Location = new System.Drawing.Point(864, 643);
            this.btnSuivant.Name = "btnSuivant";
            this.btnSuivant.Size = new System.Drawing.Size(227, 50);
            this.btnSuivant.TabIndex = 0;
            this.btnSuivant.Text = "Tour suivant";
            this.btnSuivant.UseVisualStyleBackColor = true;
            this.btnSuivant.Click += new System.EventHandler(this.btnSuivant_Click);
            // 
            // lbProjetJuste
            // 
            this.lbProjetJuste.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.lbProjetJuste.BackColor = System.Drawing.Color.White;
            this.lbProjetJuste.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbProjetJuste.Font = new System.Drawing.Font("Cursive standard", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProjetJuste.ForeColor = System.Drawing.Color.LimeGreen;
            this.lbProjetJuste.FormattingEnabled = true;
            this.lbProjetJuste.ItemHeight = 51;
            this.lbProjetJuste.Location = new System.Drawing.Point(91, 156);
            this.lbProjetJuste.Name = "lbProjetJuste";
            this.lbProjetJuste.Size = new System.Drawing.Size(297, 153);
            this.lbProjetJuste.TabIndex = 1;
            this.lbProjetJuste.SelectedIndexChanged += new System.EventHandler(this.lbProjet_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Calibri", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(454, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 35);
            this.label1.TabIndex = 2;
            this.label1.Text = "Récapitulatif";
            // 
            // lblPartenaires
            // 
            this.lblPartenaires.AutoSize = true;
            this.lblPartenaires.BackColor = System.Drawing.Color.Transparent;
            this.lblPartenaires.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartenaires.Location = new System.Drawing.Point(553, 410);
            this.lblPartenaires.Name = "lblPartenaires";
            this.lblPartenaires.Size = new System.Drawing.Size(0, 58);
            this.lblPartenaires.TabIndex = 23;
            // 
            // lblPaysStatut
            // 
            this.lblPaysStatut.AutoSize = true;
            this.lblPaysStatut.BackColor = System.Drawing.Color.Transparent;
            this.lblPaysStatut.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaysStatut.Location = new System.Drawing.Point(553, 123);
            this.lblPaysStatut.Name = "lblPaysStatut";
            this.lblPaysStatut.Size = new System.Drawing.Size(0, 58);
            this.lblPaysStatut.TabIndex = 22;
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.BackColor = System.Drawing.Color.Transparent;
            this.lblNom.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNom.Location = new System.Drawing.Point(553, 65);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(0, 58);
            this.lblNom.TabIndex = 21;
            // 
            // lblLgDossier
            // 
            this.lblLgDossier.AutoSize = true;
            this.lblLgDossier.BackColor = System.Drawing.Color.Transparent;
            this.lblLgDossier.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLgDossier.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblLgDossier.Location = new System.Drawing.Point(553, 559);
            this.lblLgDossier.Name = "lblLgDossier";
            this.lblLgDossier.Size = new System.Drawing.Size(0, 58);
            this.lblLgDossier.TabIndex = 20;
            // 
            // lblNbMembre
            // 
            this.lblNbMembre.AutoSize = true;
            this.lblNbMembre.BackColor = System.Drawing.Color.Transparent;
            this.lblNbMembre.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbMembre.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblNbMembre.Location = new System.Drawing.Point(553, 489);
            this.lblNbMembre.Name = "lblNbMembre";
            this.lblNbMembre.Size = new System.Drawing.Size(0, 58);
            this.lblNbMembre.TabIndex = 19;
            // 
            // lblCoutFinancement
            // 
            this.lblCoutFinancement.AutoSize = true;
            this.lblCoutFinancement.BackColor = System.Drawing.Color.Transparent;
            this.lblCoutFinancement.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoutFinancement.Location = new System.Drawing.Point(553, 342);
            this.lblCoutFinancement.Name = "lblCoutFinancement";
            this.lblCoutFinancement.Size = new System.Drawing.Size(0, 58);
            this.lblCoutFinancement.TabIndex = 18;
            // 
            // lblDuree
            // 
            this.lblDuree.AutoSize = true;
            this.lblDuree.BackColor = System.Drawing.Color.Transparent;
            this.lblDuree.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDuree.Location = new System.Drawing.Point(553, 265);
            this.lblDuree.Name = "lblDuree";
            this.lblDuree.Size = new System.Drawing.Size(0, 58);
            this.lblDuree.TabIndex = 17;
            // 
            // LblTheme
            // 
            this.LblTheme.AutoSize = true;
            this.LblTheme.BackColor = System.Drawing.Color.Transparent;
            this.LblTheme.Font = new System.Drawing.Font("flashback", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTheme.Location = new System.Drawing.Point(553, 193);
            this.LblTheme.Name = "LblTheme";
            this.LblTheme.Size = new System.Drawing.Size(0, 58);
            this.LblTheme.TabIndex = 16;
            // 
            // lblJuste
            // 
            this.lblJuste.AutoSize = true;
            this.lblJuste.BackColor = System.Drawing.Color.Transparent;
            this.lblJuste.Font = new System.Drawing.Font("Cursive standard", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJuste.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblJuste.Location = new System.Drawing.Point(122, 97);
            this.lblJuste.Name = "lblJuste";
            this.lblJuste.Size = new System.Drawing.Size(266, 56);
            this.lblJuste.TabIndex = 24;
            this.lblJuste.Text = "Réponses justes : ";
            // 
            // lblFaux
            // 
            this.lblFaux.AutoSize = true;
            this.lblFaux.BackColor = System.Drawing.Color.Transparent;
            this.lblFaux.Font = new System.Drawing.Font("Cursive standard", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaux.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblFaux.Location = new System.Drawing.Point(111, 312);
            this.lblFaux.Name = "lblFaux";
            this.lblFaux.Size = new System.Drawing.Size(287, 56);
            this.lblFaux.TabIndex = 25;
            this.lblFaux.Text = "Réponses fausses : ";
            // 
            // lbProjetFaux
            // 
            this.lbProjetFaux.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.lbProjetFaux.BackColor = System.Drawing.Color.White;
            this.lbProjetFaux.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbProjetFaux.Font = new System.Drawing.Font("Cursive standard", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProjetFaux.ForeColor = System.Drawing.Color.OrangeRed;
            this.lbProjetFaux.FormattingEnabled = true;
            this.lbProjetFaux.ItemHeight = 51;
            this.lbProjetFaux.Location = new System.Drawing.Point(91, 371);
            this.lbProjetFaux.Name = "lbProjetFaux";
            this.lbProjetFaux.Size = new System.Drawing.Size(297, 153);
            this.lbProjetFaux.TabIndex = 26;
            this.lbProjetFaux.SelectedIndexChanged += new System.EventHandler(this.lbProjetFaux_SelectedIndexChanged);
            // 
            // pcbChoose
            // 
            this.pcbChoose.BackColor = System.Drawing.Color.Transparent;
            this.pcbChoose.InitialImage = null;
            this.pcbChoose.Location = new System.Drawing.Point(960, 12);
            this.pcbChoose.Name = "pcbChoose";
            this.pcbChoose.Size = new System.Drawing.Size(119, 111);
            this.pcbChoose.TabIndex = 27;
            this.pcbChoose.TabStop = false;
            // 
            // iPb3
            // 
            this.iPb3.BackColor = System.Drawing.Color.Transparent;
            this.iPb3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPb3.IconChar = FontAwesome.Sharp.IconChar.Circle;
            this.iPb3.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPb3.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPb3.IconSize = 83;
            this.iPb3.Location = new System.Drawing.Point(290, 530);
            this.iPb3.Name = "iPb3";
            this.iPb3.Size = new System.Drawing.Size(83, 89);
            this.iPb3.TabIndex = 33;
            this.iPb3.TabStop = false;
            // 
            // iPb2
            // 
            this.iPb2.BackColor = System.Drawing.Color.Transparent;
            this.iPb2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPb2.IconChar = FontAwesome.Sharp.IconChar.Circle;
            this.iPb2.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPb2.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPb2.IconSize = 83;
            this.iPb2.Location = new System.Drawing.Point(201, 530);
            this.iPb2.Name = "iPb2";
            this.iPb2.Size = new System.Drawing.Size(83, 89);
            this.iPb2.TabIndex = 32;
            this.iPb2.TabStop = false;
            // 
            // iPb1
            // 
            this.iPb1.BackColor = System.Drawing.Color.Transparent;
            this.iPb1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPb1.IconChar = FontAwesome.Sharp.IconChar.Circle;
            this.iPb1.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPb1.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPb1.IconSize = 83;
            this.iPb1.Location = new System.Drawing.Point(112, 528);
            this.iPb1.Name = "iPb1";
            this.iPb1.Size = new System.Drawing.Size(83, 89);
            this.iPb1.TabIndex = 31;
            this.iPb1.TabStop = false;
            // 
            // iPbErreur3
            // 
            this.iPbErreur3.BackColor = System.Drawing.Color.Transparent;
            this.iPbErreur3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iPbErreur3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur3.IconChar = FontAwesome.Sharp.IconChar.TimesCircle;
            this.iPbErreur3.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur3.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPbErreur3.IconSize = 83;
            this.iPbErreur3.Location = new System.Drawing.Point(290, 530);
            this.iPbErreur3.Name = "iPbErreur3";
            this.iPbErreur3.Size = new System.Drawing.Size(83, 89);
            this.iPbErreur3.TabIndex = 30;
            this.iPbErreur3.TabStop = false;
            this.iPbErreur3.Visible = false;
            // 
            // iPbErreur2
            // 
            this.iPbErreur2.BackColor = System.Drawing.Color.Transparent;
            this.iPbErreur2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iPbErreur2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur2.IconChar = FontAwesome.Sharp.IconChar.TimesCircle;
            this.iPbErreur2.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur2.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPbErreur2.IconSize = 83;
            this.iPbErreur2.Location = new System.Drawing.Point(201, 530);
            this.iPbErreur2.Name = "iPbErreur2";
            this.iPbErreur2.Size = new System.Drawing.Size(83, 89);
            this.iPbErreur2.TabIndex = 29;
            this.iPbErreur2.TabStop = false;
            this.iPbErreur2.Visible = false;
            // 
            // iPbErreur1
            // 
            this.iPbErreur1.BackColor = System.Drawing.Color.Transparent;
            this.iPbErreur1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.iPbErreur1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur1.IconChar = FontAwesome.Sharp.IconChar.TimesCircle;
            this.iPbErreur1.IconColor = System.Drawing.SystemColors.ControlText;
            this.iPbErreur1.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iPbErreur1.IconSize = 83;
            this.iPbErreur1.Location = new System.Drawing.Point(112, 530);
            this.iPbErreur1.Name = "iPbErreur1";
            this.iPbErreur1.Size = new System.Drawing.Size(83, 89);
            this.iPbErreur1.TabIndex = 28;
            this.iPbErreur1.TabStop = false;
            this.iPbErreur1.Visible = false;
            // 
            // FrmRecap
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1100, 700);
            this.Controls.Add(this.iPb3);
            this.Controls.Add(this.iPb2);
            this.Controls.Add(this.iPb1);
            this.Controls.Add(this.iPbErreur3);
            this.Controls.Add(this.iPbErreur2);
            this.Controls.Add(this.iPbErreur1);
            this.Controls.Add(this.pcbChoose);
            this.Controls.Add(this.lbProjetFaux);
            this.Controls.Add(this.lblFaux);
            this.Controls.Add(this.lblJuste);
            this.Controls.Add(this.lblPartenaires);
            this.Controls.Add(this.lblPaysStatut);
            this.Controls.Add(this.lblNom);
            this.Controls.Add(this.lblLgDossier);
            this.Controls.Add(this.lblNbMembre);
            this.Controls.Add(this.lblCoutFinancement);
            this.Controls.Add(this.lblDuree);
            this.Controls.Add(this.LblTheme);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbProjetJuste);
            this.Controls.Add(this.btnSuivant);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmRecap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmRecap";
            this.Load += new System.EventHandler(this.FrmRecap_Load);
            this.Shown += new System.EventHandler(this.FrmRecap_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pcbChoose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iPbErreur1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSuivant;
        private System.Windows.Forms.ListBox lbProjetJuste;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPartenaires;
        private System.Windows.Forms.Label lblPaysStatut;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.Label lblLgDossier;
        private System.Windows.Forms.Label lblNbMembre;
        private System.Windows.Forms.Label lblCoutFinancement;
        private System.Windows.Forms.Label lblDuree;
        private System.Windows.Forms.Label LblTheme;
        private System.Windows.Forms.Label lblJuste;
        private System.Windows.Forms.Label lblFaux;
        private System.Windows.Forms.ListBox lbProjetFaux;
        private System.Windows.Forms.PictureBox pcbChoose;
        private FontAwesome.Sharp.IconPictureBox iPb3;
        private FontAwesome.Sharp.IconPictureBox iPb2;
        private FontAwesome.Sharp.IconPictureBox iPb1;
        private FontAwesome.Sharp.IconPictureBox iPbErreur3;
        private FontAwesome.Sharp.IconPictureBox iPbErreur2;
        private FontAwesome.Sharp.IconPictureBox iPbErreur1;
    }
}