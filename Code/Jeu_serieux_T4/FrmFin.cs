﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeu_serieux_T4
{
    public partial class FrmFin : Form
    {
        bool perdu;
        int nbTour;
        int nbErreur;
        int organisation;
        public FrmFin(bool p, int nbT, int nbE, int orga)
        {
            perdu = p;
            nbErreur = nbE;
            nbTour = nbT;
            organisation = orga;
            InitializeComponent();
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            try
            {
                //Le formulaire d'accueil est affiché
                fAcceuil frmAcc = new fAcceuil();
                frmAcc.Closed += (s, args) => this.Close();
                frmAcc.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FrmFin_Load(object sender, EventArgs e)
        {
            string erreurs;
            if (perdu)
            {              
                this.BackgroundImage = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\gameOver.png");
               
                lblTour.Text = "Vous avez perdu     au tour "+nbTour;
            }
            else
            {
                this.BackgroundImage = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\win.png");
                if (nbErreur <=1 )
                {
                    erreurs = "erreur";
                }
                else
                {
                    erreurs = "erreurs";
                }
                lblWin.Text = "Vous avez gagné en faisant "+nbErreur+" "+erreurs;
            }
        }

        private void FrmFin_Shown(object sender, EventArgs e)
        {
            if (organisation==0 && !perdu)
            {
                MessageBox.Show("Vous avez fini le tutoriel, vous pouvez tenter une partie normale.", "Tutoriel");
            }
            
        }
    }
}
