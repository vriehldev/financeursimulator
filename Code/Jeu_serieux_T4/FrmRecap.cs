﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeu_serieux_T4
{
    public partial class FrmRecap : Form
    {
        List<List<string>> ListP = new List<List<string>>();

        List<int> ListeFaux = new List<int>();
        List<int> ListeJuste = new List<int>();
        List<int> ListeRetenu = new List<int>();
        List<int> ListeRejeter = new List<int>();
        int organisation;
        int nbTour;
        int nbErreur;
        bool premierErreur;

        public FrmRecap(List<List<string>>ListProjet, int orga, int nbT, int nbE, List<int> LFaux, List<int> LJuste, List<int> LRetenu, List<int>LRejeter, bool e)
        {
            premierErreur = e;
            ListeRetenu = LRetenu;
            ListeRejeter = LRejeter;
            nbErreur = nbE;
            nbTour = nbT;
            ListeFaux = LFaux;
            ListeJuste = LJuste;
            ListP = ListProjet;
            organisation = orga;
            InitializeComponent();
        }

        private void FrmRecap_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < ListeJuste.Count; i++)
            {
                int id = ListeJuste.ElementAt(i);
                string projet = "Nom : " + ListP.ElementAt(id).ElementAt(0);
                lbProjetJuste.Items.Add(projet);
            }
            for (int i = 0; i < ListeFaux.Count; i++)
            {
                int id = ListeFaux.ElementAt(i);
                string projet = "Nom : " + ListP.ElementAt(id).ElementAt(0);
                lbProjetFaux.Items.Add(projet);
            }

            miseAJourErreur();

            if (nbTour==5)
            {
                btnSuivant.Text = "Fin du jeu";
            }

        }

        private void miseAJourErreur()
        {
            if (nbErreur >= 1)
            {
                iPb1.Visible = false;
                iPbErreur1.Visible = true;
            }
            if (nbErreur >= 2)
            {
                iPb2.Visible = false;
                iPbErreur2.Visible = true;
            }
            if (nbErreur >= 3)
            {
                iPb3.Visible = false;
                iPbErreur3.Visible = true;
            }
        }

        private void lbProjet_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox listbox = (ListBox)sender;
            int id = ListeJuste.ElementAt(listbox.SelectedIndex);
            majIcon(id);

            lblNom.Text = "Nom : " + ListP.ElementAt(id).ElementAt(0).ToString();
            lblPaysStatut.Text = "Pays : " + ListP.ElementAt(id).ElementAt(1).ToString() + ", statut : " + ListP.ElementAt(id).ElementAt(2).ToString();
            LblTheme.Text = ListP.ElementAt(id).ElementAt(3).ToString();
            lblDuree.Text = "Durée : " + ListP.ElementAt(id).ElementAt(4).ToString() + " mois";
            lblCoutFinancement.Text = "Coût : " + ListP.ElementAt(id).ElementAt(5).ToString() + " €, Financement : " + ListP.ElementAt(id).ElementAt(6).ToString() + " %";
            lblPartenaires.Text = "Nombre de partenaires : " + ListP.ElementAt(id).ElementAt(7).ToString() + " partenaires";
            lblNbMembre.Text = "Nombre de membres : " + ListP.ElementAt(id).ElementAt(8).ToString() + " membres";
            lblLgDossier.Text = "Longueur du dossier : " + ListP.ElementAt(id).ElementAt(9).ToString() + " pages";
        }

        private void btnSuivant_Click(object sender, EventArgs e)
        {
            if (nbTour == 5 || (nbTour==2 && organisation==0))
            {      
                FrmFin frm = new FrmFin(false, nbTour, nbErreur, organisation);
                frm.Closed += (s, args) => this.Close();
                frm.ShowDialog();
            }

            nbTour++;

            // Un nouveau formulaire de décision est créé           
            FrmProjet frmP = new FrmProjet(organisation, nbTour, nbErreur, premierErreur);
            frmP.Closed += (s, args) => this.Close();
            frmP.ShowDialog();

            
        }

        private void lbProjetFaux_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox listbox = (ListBox)sender;
            int id = ListeFaux.ElementAt(listbox.SelectedIndex);
            majIcon(id);

            lblNom.Text = "Nom : " + ListP.ElementAt(id).ElementAt(0).ToString();
            lblPaysStatut.Text = "Pays : " + ListP.ElementAt(id).ElementAt(1).ToString() + ", statut : " + ListP.ElementAt(id).ElementAt(2).ToString();
            LblTheme.Text = ListP.ElementAt(id).ElementAt(3).ToString();
            lblDuree.Text = "Durée : " + ListP.ElementAt(id).ElementAt(4).ToString() + " mois";
            lblCoutFinancement.Text = "Coût : " + ListP.ElementAt(id).ElementAt(5).ToString() + " €, Financement : " + ListP.ElementAt(id).ElementAt(6).ToString() + " %";
            lblPartenaires.Text = "Nombre de partenaires : " + ListP.ElementAt(id).ElementAt(7).ToString() + " partenaires";
            lblNbMembre.Text = "Nombre de membres : " + ListP.ElementAt(id).ElementAt(8).ToString() + " membres";
            lblLgDossier.Text = "Longueur du dossier : " + ListP.ElementAt(id).ElementAt(9).ToString() + " pages";           
        }

        private void majIcon(int id)
        {
            if (ListeRetenu.Contains(id))
            {
                pcbChoose.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbChoose.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\v.png");
            }
            else
            {
                pcbChoose.SizeMode = PictureBoxSizeMode.StretchImage;
                pcbChoose.Image = System.Drawing.Image.FromFile(System.Windows.Forms.Application.StartupPath + @"\Images\x.png");
            }
        }

        private void FrmRecap_Shown(object sender, EventArgs e)
        {
            if (organisation == 0 && nbTour == 1)
            {
                MessageBox.Show("Vous voici maintenant sur le formulaire de récapitulation du tour.", "Tutoriel");
                MessageBox.Show("Vous trouverez sur l'ardoise à gauche la liste de vos réponses justes et de vos erreurs.", "Tutoriel");
                MessageBox.Show("Il est également inscrit, en dessous de ces listes, le nombre d'erreurs commises sur la totalité de la partie.", "Tutoriel");
                MessageBox.Show("Si vous le souhaitez, vous pouvez retrouver les détails d'un projet sur la feuille à la droite de votre écran en cliquant sur celui-ci dans une des listes.", "Tutoriel");
                MessageBox.Show("Vous pouvez passer au tour suivant en cliquant sur le bouton Tour suivant.");
            }
        }
    }
}
