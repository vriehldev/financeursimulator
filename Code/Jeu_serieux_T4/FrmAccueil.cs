﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jeu_serieux_T4
{
    public partial class fAcceuil : Form
    {
        int organisation = 0;
        int nbTour = 1;
        int nbErreur = 0;
        bool premierErreur = true;

        public fAcceuil()
        {
            InitializeComponent();
        }

        private void btnEuropeCreative_Click(object sender, EventArgs e)
        {
            try
            {
                organisation = 1;
                //Le formulaire d'accueil est caché
                this.Hide();
                //Un nouveau formulaire de décision est créé
                FrmProjet newForm = new FrmProjet(organisation, nbTour, nbErreur, premierErreur);
                newForm.Closed += (s, args) => this.Close();
                newForm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void fAcceuil_Load(object sender, EventArgs e)
        {

        }

        private void btnTuto_Click(object sender, EventArgs e)
        {
            try
            {           
                //Le formulaire d'accueil est caché
                this.Hide();
                //Un nouveau formulaire de décision est créé
                FrmProjet newForm = new FrmProjet(organisation, nbTour, nbErreur,premierErreur);
                newForm.Closed += (s, args) => this.Close();
                newForm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void iPbRegles_Click(object sender, EventArgs e)
        {
            Form formBackground = new Form();
            try
            {
                using (FrmAideOrga uu = new FrmAideOrga(-1))
                {
                    formBackground.StartPosition = FormStartPosition.Manual;
                    formBackground.FormBorderStyle = FormBorderStyle.None;
                    formBackground.Opacity = .50d;
                    formBackground.BackColor = Color.Black;
                    formBackground.WindowState = FormWindowState.Maximized;
                    formBackground.Location = this.Location;
                    formBackground.ShowInTaskbar = false;
                    formBackground.Show();

                    uu.Owner = formBackground;
                    uu.ShowDialog();

                    formBackground.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                formBackground.Dispose();
            }
        }
    }
}
