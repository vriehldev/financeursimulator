# Présentation du projet 

Notre jeu est constitué : 

- d'un menu principal :

![Menu principal](Captures/menu.png)

- de règles :

![Règles](Captures/regle.png)

- d'un formulaire définissant les critères d'une organisation : 

![Critères d'une organisation](Captures/regleOrga.png)

- de l'interface de base pour un projet et un tour donnés : 

![Projet](Captures/projet.png)

- d'une interface de récapitulation entre chaque tour : 

![Récapitulatif](Captures/recap.png)

- d'un écran de fin de partie : 

![Ecran de fin](Captures/fin.png)

Vous incarnerez **un membre d'une organisation de financement** responsable du choix des projets à financer dont le but est de **retenir ou éliminer des projets** demandant un financement. Ce choix doit être fait en fonction des **critères de sélection** propre à chaque organisation de financement. 

# Le cahier des charges 

Vous trouverez dans [le cahier des charges](/CDC.md) des informations complémentaires sur le jeu (objectifs pédagogiques, description du jeu, scénario,...).

Pour des raisons de facilités, les commantaires des S2 se feront sur le [GDoc](https://docs.google.com/document/d/1v28v_aJ5sGPblcblMyxX0wZTTBlk14Dww5bKphnnf24/edit#heading=h.p1l7u12dgulp) (copie exacte du dossier Cdc.md)

# Grille d'évalution

Les évaluations de chaque membre de T2 sont disponibles dans le dossier [Evaluations_T2](./Evaluations_T2) : 
- [Julien-Alexandre BERTIN-KLEIN](./Evaluations_T2/Evaluation-T4-Julien-Alexandre_Bertin-Klein.md)
- [Philippe OSTER](./Evaluations_T2/Evaluation-T4_Philippe_Oster.md)
- [Noe KIEFFER](./Evaluations_T2/Evaluation-T4_Noe_Kieffer.md)
- [Melissa FOECHTERLE](./Evaluations_T2/Evaluation-T4-Melissa_Foechterle.md)
- [Quentin WARTH](./Evaluations_T2/Evaluation-T4_Quentin_Warth.md)

# Installation de l'application 

1. Télécharger le dossier [Installation](/Installation) en .zip.
2. Extraire l'archive sur votre ordinateur.
2. Lancer le fichier **FinancementSimulator.msi**.  
3. Suivre les étapes de l'installation, et choisir le répertoire d'installation.

# Exécution de l'application

1. Ouvrir le fichier **FinanceurSimulator.exe** se trouvant dans le repertoire d'installation.
2. Si l'installation s'est bien déroulée vous devriez arriver sur la fenêtre du menu principal.

# Code source de l'application 

Le code source de l'application est disponible [ici](/Code/Jeu_serieux_T4).

# Outils utilisés

- [Visual Studio 2019](https://visualstudio.microsoft.com/fr/)
- [Microsoft Access](https://www.microsoft.com/fr-fr/microsoft-365/p/access/cfq7ttc0k7q8?activetab=pivot%3aoverviewtab)
- [Installer Project](https://marketplace.visualstudio.com/items?itemName=VisualStudioClient.MicrosoftVisualStudio2017InstallerProjects#overview)
- [Font Awesome](https://github.com/awesome-inc/FontAwesome.Sharp)
- [Dafont](https://www.dafont.com/fr/)

# Auteurs 

- @flejou
- @vriehl
- @yberthier
- @bayi


